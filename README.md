# atlas-connect

A command line utility for the
[`atlassian-connect-express`](https://bitbucket.org/atlassian/atlassian-connect-express) library.

`atlas-connect` makes it easy to create [Atlassian Connect](https://developer.atlassian.com/connect)
apps for:

* [Bitbucket](https://bitbucket.org)
* [Confluence](https://atlassian.com/confluence)
* [JIRA](https://atlassian.com/jira)
* [JIRA Service Desk](https://www.atlassian.com/service-desk)

`atlas-connect` requires Node.js v4.8.4 or later. Please also ensure you have relevant [security updates](https://nodejs.org/en/blog/vulnerability/) installed.

You can install me with:

```
npm i -g atlas-connect
```

## Usage

`atlas-connect` provides scaffolding for Atlassian Connect apps that use
[`atlassian-connect-express`](https://bitbucket.org/atlassian/atlassian-connect-express).

To get started, simply run:

```
atlas-connect new
```

The generated scaffold bundles the
[atlassian-connect-express](https://bitbucket.org/atlassian/atlassian-connect-express) package which
greatly simplifies the process of creating remote apps.

## Getting Help or Support

You can learn more about Atlassian Connect via the [Atlassian Connect
documentation](https://developer.atlassian.com/connect) or report issues in the [Atlassian Connect
Express project](https://ecosystem.atlassian.net/browse/ACEJS).

## Contributing

Even though this is just an exploratory project at this point, it's also open source [Apache 2.0](https://bitbucket.org/atlassian/node-atlas-connect/src/master/LICENSE.txt). So, please feel free to fork and send us pull requests.

## Release process

Pre-requisites: be an owner of https://npmjs.org/package/atlas-connect

1. npm version <major.minor.patch>
2. npm publish
