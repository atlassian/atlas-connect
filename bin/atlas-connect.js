#!/usr/bin/env node

var program = require('commander');
var path = require('path');
var updateNotifier = require('update-notifier');

var npmPackage = require(path.join(__dirname, '../package.json'));

updateNotifier({ pkg: npmPackage }).notify({ defer: true });

program
    .version(npmPackage.version)
    .command('new [name]', 'create a new Atlassian Connect app')
    .command('configure', 'configure Connect dev environment')
    .parse(process.argv);
