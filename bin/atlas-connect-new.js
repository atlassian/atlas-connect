#!/usr/bin/env node

var program = require('commander');
var extend = require('extend');
var inquirer = require('inquirer');
var path = require('path');

var generator = require(path.join(__dirname, '..', 'lib', 'generator'));

var templatePattern = /^(jira|jira-service-desk|confluence|bitbucket|https?:\/\/.*)$/;
program
    .option('-t, --template <template>', 'the template to use (jira|jira-service-desk|confluence|bitbucket) or the url to download a custom template .zip from')
    .parse(process.argv);

var args = program.args;
var name = null;
if (args && args.length > 0) {
    name = args[0];
}

var template = program.template;

if (typeof template !== 'string') {
    template = null;
} else if (template) {
    template = template.toLowerCase();
    if (!templatePattern.test(template)) {
        generator.logError('The template requested is invalid. Please select from one of (jira|jira-service-desk|confluence|bitbucket)' +
        ' or provide an https:// url to a custom template .zip file.');
        return;
    }
}

var opts = {
    name: name,
    template: template
};
var questions = [];

if (!name) {
    questions.push({
        type: 'input',
        name: 'name',
        message: 'What would you like to name your project?',
        validate: function (input) {
            if (input.trim().length === 0) {
                return 'Please enter a non empty value';
            }
            return true;
        },
        filter: function (value) {
            return value.trim();
        }
    });
}

if (!template) {
    questions.push({
        type: 'list',
        name: 'template',
        message: 'Select a product',
        choices: ['jira', 'jira-service-desk', 'confluence', 'bitbucket']
    });
}

var promise;
if (questions.length) {
    promise = inquirer.prompt(questions);
} else {
    promise = Promise.resolve([]);
}

promise.then(function (answers) {
    opts = extend(opts, answers);
    generator.createTemplate(opts.name, opts.template);
});
