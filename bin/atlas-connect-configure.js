#!/usr/bin/env node

const inquirer = require('inquirer');
const { join } = require('path');
const { execSync } = require('child_process');

const setupNgrok = (authToken) => {
    try {
        // Get the node_modules from the working directory, which should be the root connect modules directory
        execSync(`${join(process.cwd(), 'node_modules/.bin/ngrok')} authtoken ${authToken}`);
        console.log('Successfully configured Ngrok!');
    } catch (error) {
        console.error('Could not configure Ngrok:\n', error);
        process.exit(1);
    }
}

const run = () => {
    console.log('Hello! Let\'s set up your very own Connect dev environment! ✨ ');

    inquirer.prompt({
        // Configure Ngrok
        type: 'password',
        name: 'authToken',
        message: 'Please enter your Ngrok auth token. See https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/#step-3--set-up-your-local-development-environment for more info.',
        validate: (input) => {
            if (input.trim().length === 0) {
                return 'Please enter a non empty value';
            }
            return true;
        }
    }).then(({ authToken }) => {
        setupNgrok(authToken);
        process.exit(0);
    });
}

run();